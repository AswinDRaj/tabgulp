(function () {
  'use strict';

  angular
    .module('tapWeb', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize','ngStorage', 'ngMessages','ngResource', 'ngAria', 'ui.router', 'ui.bootstrap', 'toastr']);
})();
