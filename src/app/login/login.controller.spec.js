(function() {
  'use strict';

  describe('controllers', function(){
    var vm;
    var $timeout;
    var toastr;
    var $rootScope;
    var $routeParams;
    var $scope;
    var $http;
    var $location;
    var $window;

    beforeEach(module('tapWeb'));
    beforeEach(inject(function(_$controller_, _$timeout_, _webDevTec_, _toastr_,$rootScope,$routeParams,$scope,$http,$location,$timeout,$window) {
      spyOn(_webDevTec_, 'getTec').and.returnValue([{}, {}, {}, {}, {}]);
      spyOn(_toastr_, 'info').and.callThrough();

      vm = _$controller_('LoginController');
      $timeout = _$timeout_;
      toastr = _toastr_;
      $rootScope = $rootScope;
      $routeParams = $routeParams;
      $scope = $scope;
      $http = $http;
      $location = $location;
      $window = $window;
    }));
    it('should have a timestamp creation date', function() {
      expect(vm.creationDate).toEqual(jasmine.any(Number));
    });

    it('should define animate class after delaying timeout ', function() {
      $timeout.flush();
      expect(vm.classAnimation).toEqual('rubberBand');
    });

    it('should show a Toastr info and stop animation when invoke showToastr()', function() {
      vm.showToastr();
      expect(toastr.info).toHaveBeenCalled();
      expect(vm.classAnimation).toEqual('');
    });

    it('should define more than 5 awesome things', function() {
      expect(angular.isArray(vm.awesomeThings)).toBeTruthy();
      expect(vm.awesomeThings.length === 5).toBeTruthy();
    });
  });
})();
